const AWS = require('aws-sdk');
const Slack = require('slack-node');
const moment = require('moment-timezone');

const DEFAULT_VALUE = 'not defined';
const DEFAULT_ERROR_TYPE = 'RUNTIME';
const slack = new Slack();
const slackURL = process.env.SLACK || 'https://hooks.slack.com/services/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';

slack.setWebhook(slackURL);

moment.tz.add('America/Sao_Paulo|LMT -03 -02|36.s 30 20|012121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212121212|-2glwR.w HdKR.w 1cc0 1e10 1bX0 Ezd0 So0 1vA0 Mn0 1BB0 ML0 1BB0 zX0 pTd0 PX0 2ep0 nz0 1C10 zX0 1C10 LX0 1C10 Mn0 H210 Rb0 1tB0 IL0 1Fd0 FX0 1EN0 FX0 1HB0 Lz0 1EN0 Lz0 1C10 IL0 1HB0 Db0 1HB0 On0 1zd0 On0 1zd0 Lz0 1zd0 Rb0 1wN0 Wn0 1tB0 Rb0 1tB0 WL0 1tB0 Rb0 1zd0 On0 1HB0 FX0 1C10 Lz0 1Ip0 HX0 1zd0 On0 1HB0 IL0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1zd0 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0 On0 1zd0 On0 1zd0 On0 1C10 Lz0 1C10 Lz0 1C10 Lz0 1C10 On0 1zd0 Rb0 1wp0 On0 1C10 Lz0 1C10 On0 1zd0|20e6');
moment.tz.setDefault('America/Sao_Paulo');

AWS.config.update({
  region: process.env.AWS_REGION,
  accessKeyId: process.env.AWS_KEYID,
  secretAccessKey: process.env.AWS_ACCESSKEY
});

const SQS_TARGET = process.env.AWS_SQS_ERROR;
const sqs = new AWS.SQS();

const sendToSQS = (data) => {
  return new Promise((resolve, reject) => {
    const params = {
      MessageBody: JSON.stringify(data),
      QueueUrl: SQS_TARGET
    };

    sqs.sendMessage(params, (err) => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

const toErrorInfo = (error) => ({
  message: error.message || DEFAULT_VALUE,
  type: error.type || DEFAULT_ERROR_TYPE,
  stacktrace: error.stack || DEFAULT_VALUE
});

/**
 * Send Error Message to Slack
 */
const sendSlackMessage = (error, eventId, sentDate) => {
  const report = `*event*: ${eventId || DEFAULT_VALUE},
  *type*: ${error.type || DEFAULT_ERROR_TYPE},
  *message*: \`\`\`${error.message || DEFAULT_VALUE}\`\`\`
  *sentTime*: ${sentDate ? moment(sentDate).utc().format('YYYY-MM-DD') : DEFAULT_VALUE},
  *processedTime*: ${moment().toISOString(true)}`;

  slack.webhook({
    text: report
  }, (err) => {
    if (err) { console.error(err); }
  });
};

/**
 * Handles Application error saving its information on database
 */
const handleError = (error, data) => {
  return new Promise((resolve, reject) => {
    data.error = toErrorInfo(error);

    if (data && data.dynamoId) {
      sendSlackMessage(error, data.dynamoId, data.sent_date);
    }
    // If data does not have the property dynamoId it means that is not a message
    else {
      sendSlackMessage(error);
    }

    if (!data.error || data.error.type === DEFAULT_ERROR_TYPE) {
      resolve();
    } else {
      sendToSQS(data)
        .then(() => resolve())
        .catch((err) => reject(err));
    }
  });
};

module.exports.handleError = handleError;
